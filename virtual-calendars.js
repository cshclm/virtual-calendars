/*
 * Virtual Calendars Google Script
 *
 * This script turns a calendar into a `Virtual Calendar'.  A Virtual Calendar
 * is one that contains a view of the events from one or more other calendars.
 * Events published to this calendar will be copied to another calendar.
 *
 * Features:
 *  - Control over the visibility of the copied events and the text for free/busy events.
 *  - Control whether All Day events are copied
 *  - Title prefixes to help identify which events are `virtual'
 *  - Set the start date and number of days to synchronise.
 *  - Define work hours, outside of which event copying does not occur
 *  - Copy events added in to the virtual calendar to a "copy down" calendar
 *
 * Events already in the Virtual Calendar (i.e., those which are not `virtual')
 * will not be changed or removed.
 * All calendars used must be accessible from the single google account.
 *
 * This is a Google App Script file.
 * See https://developers.google.com/apps-script/ for details.
 *
 * Author: Chris Mann <cshclm@gmail.com>
 */

/*
 * Calendar Configuration
 * This configuration must be edited.
 */
var config = {
    debug: true,
    // The calendar where events from other calendars will be moved.
    virtualCalendarId: "myemail@gmail.com",
    // The calendar to which events on the virtual calendar, but which were
    // not created by the Virtual Calendar script, will be copied.  i.e., this
    // calendar will be populated with events copied FROM the virtual
    // calendar.
    copyDownCalendarId: "<your calendar ID>",
    // The synchronisation works for events in a certain date range.  This
    // sets the beginning of the range.  use `new Date()' to start from Now!
    syncStartDate: new Date(),
    // The number of days after syncStartDate to include in the event
    // synchronisation.  There are limitations with repeating tasks; should
    // not exceed a 'normal' repeat schedule.
    daysToSync: 7,
    // Prefix to prepend to every title
    titlePrefix: "[",
    titleSuffix: "]",
    // Title text to display for 'freebusy' event visibility.  (See below)
    busyText: "Unavailable",
    // Whether work hours (see below) should be enabled
    enableWorkHours: true,
    // The days and range of hours where events are eligible to be copied.
    // Hours apply to the start time of an event only.
    workHours: {
        'mon': {
            start: 7,
            end: 18
        },
        'tue': {
            start: 7,
            end: 18
        },
        'wed': {
            start: 7,
            end: 18
        },
        'thu': {
            start: 7,
            end: 18
        },
        'fri': {
            start: 7,
            end: 18
        }
    },
    // Calendars.  The 'key' is the calendar ID (available through settings).
    // The eventVisibility for each calendar must be set.  These calendar will
    // be copied TO the virtual calendar.
    sourceCalendars: {
        // e.g., work
        "source-calendar-1": {
            syncAllDayEvents: false,
            eventVisibility: "full"
        },
        // e.g., personal
        "source-calendar-2": {
            syncAllDayEvents: false,
            eventVisibility: 'freebusy'
        }
    },
    // The visibility setting to use by default, should one not be set explicitly for a
    // sourceCalendar.
    defaultVisibility: "full",
    defaultSyncAllDayEvents: true
};

/*
 * IMPLEMENTATION
 */

var compose = function () {
    var fns = arguments;
    return function (result) {
        for (var i = fns.length - 1; i >= 0; i--) {
            if (result != null) {
                result = fns[i].call(this, result);
            }
        }
        return result;
    };
};

var debug = function(str) {
    if (config.debug) {
        Logger.log(str);
    }
};

function main() {
    const IDENTIFIER = "VCAL-ID";
    var builder = function(c) {
        var endDate = function() {
            var end = new Date(c.syncStartDate);
            end.setDate(c.syncStartDate.getDate() + c.daysToSync);
            return end;
        };

        var nextAction = function(event) {
            for (var i = 0; i < _virtEvents.length; i++) {
                if (_virtEvents[i].event.getTag(IDENTIFIER) === event.getId()) {
                    Logger.log("Registering update event");
                    return {
                        type: "update",
                        node: _virtEvents[i],
                        target: _virtEvents[i].event,
                        source: event
                    };
                }
            }
            Logger.log("Registering create event");
            return {
                type: "create",
                node: _virtEvents[i],
                source: event
            };
        };

        var getEventTitle = function(event, permissions) {
            if (permissions === "full") {
                return c.titlePrefix + event.getTitle() + c.titleSuffix;
            } else {
                return c.titlePrefix + c.busyText + c.titleSuffix;
            }
        };

        var getEventDescription = function(event, permissions) {
            if (permissions === "full") {
                return event.getDescription();
            } else {
                return "";
            }
        };
        
        var getEventLocation = function(event, permissions) {
            if (permissions === "full") {
                return event.getLocation();
            } else {
                return "";
            }
        };

        var createEvent = function(calendar, event, workHours, settings) {
            debug("Creating new calendar event");
            var permissions = settings.eventVisibility == null ? c.defaultEventVisibility : settings.eventVisibility;
            var syncAllDayEvents = settings.syncAllDayEvents == null ? c.defaultSyncAllDayEvents : settings.syncAllDayEvents;
            var evt;
            if (event.isAllDayEvent()) {
                if (syncAllDayEvents && (!c.enableWorkHours || workHours[event.getAllDayStartDate().getDay()].workDay)) {
                    evt = calendar.createAllDayEvent(getEventTitle(event, permissions),
                                                     event.getAllDayStartDate(),
                                                     { description: getEventDescription(event, permissions) });
                }
            } else if (event.isRecurringEvent()) {
              var day = workHours[event.getStartTime().getDay()];
              if (!c.enableWorkHours || (day.workDay &&
                     event.getStartTime().getHours() >= day.start &&
                     event.getStartTime().getHours() <= day.end)) {
                evt = calendar.createEvent(getEventTitle(event, permissions),
                                           event.getStartTime(),
                                           event.getEndTime(),
                                           { description: getEventDescription(event, permissions) });
                }
            } else {
              var day = workHours[event.getStartTime().getDay()];
              if (!c.enableWorkHours || (day.workDay &&
                     event.getStartTime().getHours() >= day.start &&
                     event.getStartTime().getHours() <= day.end)) {
                evt = calendar.createEvent(getEventTitle(event, permissions),
                                           event.getStartTime(),
                                           event.getEndTime(),
                                           { description: getEventDescription(event, permissions) });
                }
            }
            if (evt) {
                if (evt.getLocation() !== event.getLocation() && permissions === 'full') {
                    evt.setLocation(event.getLocation());
                }
                evt.setTag(IDENTIFIER, event.getId());
            }
            return evt;
        };

        var updateEvent = function(targetEvt, sourceEvt, workHours, settings) {
            var permissions = settings.eventVisibility == null ? c.defaultEventVisibility : settings.eventVisibility;
            var syncAllDayEvents = settings.syncAllDayEvents == null ? c.defaultSyncAllDayEvents : settings.syncAllDayEvents;
            if (sourceEvt.isAllDayEvent()) {
                if (syncAllDayEvents && (!c.enableWorkHours || workHours[sourceEvt.getAllDayStartDate().getDay()].workDay)) {
                    if (targetEvt.getAllDayStartDate().getTime() !== sourceEvt.getAllDayStartDate().getTime()) {
                        debug("Updating all day date");
                        targetEvt.setAllDayDate(sourceEvt.getAllDayStartDate());
                    }
                } else {
                    return false;
                }
            } else {
                var day = workHours[sourceEvt.getStartTime().getDay()];
                if (c.enableWorkHours && (!day.workDay ||
                    (day.workDay &&
                     (sourceEvt.getStartTime().getHours() < day.start ||
                      sourceEvt.getStartTime().getHours() > day.end)))) {
                    debug('Event has moved outside of work hours.  Removing.');
                    return false;
                }
                if (targetEvt.getStartTime().getTime() !== sourceEvt.getStartTime().getTime() ||
                    targetEvt.getEndTime().getTime() !== sourceEvt.getEndTime().getTime()) {
                    sourceEvt.getEndTime().getTime();
                    targetEvt.setTime(sourceEvt.getStartTime(), sourceEvt.getEndTime());
                }
            }
            if (targetEvt.getDescription() !== getEventDescription(sourceEvt, permissions)) {
                debug("Updating event description");
                targetEvt.setDescription(getEventDescription(sourceEvt, permissions));
            }
            if (targetEvt.getTitle() !== getEventTitle(sourceEvt, permissions)) {
                debug("Updating event title");
                targetEvt.setTitle(getEventTitle(sourceEvt, permissions));
            }
            if (targetEvt.getLocation() !== getEventLocation(sourceEvt, permissions)) {
                targetEvt.setLocation(getEventLocation(sourceEvt, permissions));
            }
            return true;
        };

        var maybeDelete = function(virtEvent) {
            var tag = virtEvent.event.getTag(IDENTIFIER);
            if (!virtEvent.found && tag && tag.length > 0) {
                debug("Deleting an event");
                virtEvent.event.deleteEvent();
            }
        };

        var runAction = function(workHours, settings) {
            return function(action) {
                var perserve;
                debug("Running the action");
                if (action.type === "create") {
                    preserve = createEvent(_virtCalendar, action.source, workHours, settings);
                } else if (action.type === "update") {
                    preserve = updateEvent(action.target, action.source, workHours, settings);
                    if (preserve) {
                        action.node.found = true;
                    }
                } else {
                    Logger.log("Unknown action type: " + action.type);
                    return null;
                }
                return action;
            };
        };
        
        function generateWorkHours() {
            var days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
            var times = [];
            for (var i = 0; i < days.length; i++) {
                var day = config.workHours[days[i]];
                if (day) {
                    day.workDay = true;
                } else {
                    day = { workDay: false };
                }
                times[i] = day;
            }
            return times;
        };

        var publ = {
            getCalendar: function(calendarId) {
                var cal = CalendarApp.getCalendarById(calendarId);
                if (!cal) {
                    Logger.log("[WARNING] Unable to find calendar: " + calendarId);
                }
                return cal;
            },

            retrieveCalendarEvents: function(calendar) {
                debug("Fetching calendar events");
                return {
                    calendar: calendar,
                    events: calendar.getEvents(c.syncStartDate, endDate())
                };
            },

            syncEventList: function(calEvents) {
                debug("Synchronising the event list");
                var cal = calEvents.calendar;
                var events = calEvents.events;
                var workHours = generateWorkHours();
                debug("Synchronising " + events.length + " events");
                events.map(compose(
                    runAction(workHours, c.sourceCalendars[cal.getId()]),
                    nextAction
                ));
            },

            excludeVirtualEvents: function(events) {
                events.events = events.events.filter(
                    function(e) {
                        var vId = e.getTag(IDENTIFIER);
                        return (vId == "" || vId == null);
                    }
                );
                return events;
            },

            deleteMissing: function() {
                _virtEvents.map(maybeDelete);
            },

            virtualEvents: function() {
                return _virtEvents;
            },

            virtualCalendar: function() {
                return _virtCalendar;
            }
        };

        var _virtCalendar = publ.getCalendar(c.virtualCalendarId);
        if (!_virtCalendar) {
            throw "Unable to find virtual calendar";
        }
        var _virtEvents   = publ.retrieveCalendarEvents(_virtCalendar)
            .events
            .map(function(x) {
                return {
                    found: false,
                    event: x
                };
            });
        return publ;
    };

    function syncCalendar(config) {
        var b = builder(config);
        if (!b.virtualCalendar()) { return; }

        Object.keys(config.sourceCalendars)
            .filter(function(k) { return config.sourceCalendars.hasOwnProperty(k); })
            .map(compose(b.syncEventList,
                         b.excludeVirtualEvents,
                         b.retrieveCalendarEvents,
                         b.getCalendar));
        b.deleteMissing();
    }

    function buildCopyDownConfig(config) {
        var cdc = {
            debug: config.debug,
            virtualCalendarId: config.copyDownCalendarId,
            syncStartDate: config.syncStartDate,
            daysToSync: config.daysToSync,
            busyText: config.busyText,
            titlePrefix: '',
            titleSuffix: '',
            sourceCalendars: {},
            defaultVisibility: config.defaultVisibility,
            defaultSyncAllDayEvents: config.defaultSyncAllDayEvents,
            enableWorkHours: config.enableWorkHours,
            workHours: config.workHours
        };
        cdc.sourceCalendars[config.virtualCalendarId] = {
            eventVisibility: 'full',
            syncAllDayEvents: true
        };
        return cdc;
    }

    syncCalendar(config);
    debug("Doing CopyDown");
    var copyDownConfig = buildCopyDownConfig(config);
    syncCalendar(copyDownConfig);
    debug("Complete");
};

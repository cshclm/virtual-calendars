# Virtual Calendars

A Google Apps script that combines events from multiple calendars into a
single virtual calendar.

## Intro

A Virtual Calendar is one that contains a view of the events from one or more
other calendars of your choosing.

The virtual calendar may have events added to it by yourself or others. The
events published to this calendar will also be copied into another calendar of
your choosing.

Events already in the Virtual Calendar (i.e., those which are not `virtual')
will not be changed or removed.  All calendars used must be accessible from
the single google account.

## Features

* Control over the visibility of the copied events and the text for free/busy events.
* Control whether All Day events are copied
* Title prefixes to help identify which events are "virtual"
* Set the start date and number of days to synchronise.
* Define work hours, outside of which event copying does not occur
* Copy events added in to the virtual calendar to a "copy down" calendar

## Installation

Configure the `config` variable at the top of the `virtual-calendars.js` file.

This is a Google App Script file that can be installed in to your Google
account.  See https://developers.google.com/apps-script/ for details.
